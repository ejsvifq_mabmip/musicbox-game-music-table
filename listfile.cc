#include"mci/mci.h"
#include<fstream>
#include<exception>
#include<iostream>
#include<string>
#include<iomanip>
#include<string>
#include<algorithm>
#include<vector>
#include<tuple>
#include<string_view>
#include<chrono>

int main(int argc,char **argv) 
try
{
	std::ifstream fin("listfile.csv");
	if(!fin)
		fin.exceptions(std::ifstream::failbit);
	using namespace std::literals;
	auto const prefix(";sound/music/"s);
	std::vector<std::tuple<std::string,std::string,std::string,std::chrono::milliseconds>> vec;
	for(std::string line;getline(fin,line);)
	{
		std::size_t const prefix_pos(line.find(prefix));
		if(prefix_pos!=std::string::npos)
			try
			{
				auto mline(line.substr(prefix_pos+1));
				mci::mci p("mpegaudio"s,mline);
				auto pos(line.rfind('/'));
				vec.emplace_back(line.substr(prefix_pos+1,pos-prefix_pos-1),line.substr(0,prefix_pos-1),line.substr(pos+1),p.duration());
			}
			catch(std::exception const& e)
			{
				std::clog<<line<<":\t\t"<<e.what()<<'\n';
			}
	}
	sort(vec.begin(),vec.end());
	std::ofstream fout("listfile.lua",std::ofstream::binary);
	fout<<std::fixed<<std::setprecision(3);
	fout<<
R"acef(local MB=LibStub("AceAddon-3.0"):GetAddon("MusicBox")
local atp=MB.AddTempPlaylist
)acef";
	std::string s;
	for(const auto &ele : vec)
	{
		if(std::get<0>(ele)==s)
			fout<<",{";
		else
		{
			if(!s.empty())
				fout<<"})\n";
			s=std::get<0>(ele);
			fout<<"atp(MB,\""<<std::get<0>(ele)<<"\",{{";
		}
		fout<<std::get<1>(ele)<<","<<std::chrono::duration_cast<std::chrono::duration<double>>(std::get<3>(ele)).count()<<",\""<<std::get<2>(ele)<<"\"}";
	}
	if(!s.empty())
		fout<<"})\n";

}
catch(const std::exception &e)
{
	std::cerr<<e.what()<<'\n';
	return 1;
}
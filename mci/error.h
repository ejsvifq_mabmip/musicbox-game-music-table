#ifndef MCI_ERROR_H
#define MCI_ERROR_H

#include<string>
#include<stdexcept>
#include<array>
#include<system_error>

namespace mci
{
	template<typename T>
	std::string get_error_string(const T& v)
	{
		std::array<char,129> c_style_string_buffer;
		if(!mciGetErrorString(v,c_style_string_buffer.data(),c_style_string_buffer.size()))
			throw std::runtime_error("mciGetErrorString failed");
		return c_style_string_buffer.data();
	}
}

#endif
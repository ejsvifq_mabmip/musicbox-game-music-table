#ifndef MCI_API_WRAPPER_H
#define MCI_API_WRAPPER_H

#include"error.h"

namespace mci
{
	template<typename... Args>
	void send_string(const std::string& name,Args&& ...args)
	{
		auto ret(mciSendString(name.c_str(),std::forward<Args>(args)...));
		if(ret)
			throw std::system_error(std::error_code(ret,std::system_category()),name + " : " + get_error_string(ret));
	}
	
	void send_std_string(const std::string& message)
	{
		send_string(message,nullptr,0,nullptr);
	}
	
	std::string sg_std_string(const std::string& message)
	{
		std::array<char,4096> c_style_string_buffer;
		send_string(message,c_style_string_buffer.data(),c_style_string_buffer.size(),nullptr);
		return c_style_string_buffer.data();
	}
	
	template<typename... Args>
	void send_command(Args&& ...args)
	{
		auto ret(mciSendCommand(std::forward<Args>(args)...));
		if(ret)
			throw std::system_error(std::error_code(ret,std::system_category()),get_error_string(ret));
	}
}
#endif